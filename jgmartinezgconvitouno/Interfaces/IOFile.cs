﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jgmartinezgconvitouno.Interfaces
{
    public interface IOFile<T>
    {
        T ReadObjectFromXml(string filePath);
        void WriteObjectFromXml(string filePath);
    }
}
