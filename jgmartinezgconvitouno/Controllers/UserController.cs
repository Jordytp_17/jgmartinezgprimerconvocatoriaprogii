﻿using jgmartinezgconvitouno.Views;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace jgmartinezgconvitouno.Controllers
{
    class UserController
    {
        private User Users;
        private SaveFileDialog Savedialog;
        public UserController (User users)
        {
            Users = users;
            Savedialog = new SaveFileDialog();
            InitController();
        }
        private void InitController()
        {
            Users.BtnCancelUser.Click += new RoutedEventHandler(this.UserEventHandler);
            Users.BtnSaveUser.Click += new RoutedEventHandler(this.UserEventHandler);
        }
        private void UserEventHandler(object sender, RoutedEventArgs args)
        {
            Button Btn = (Button)sender;
            switch (Btn.Name)
            {
                case "BtnCancelUser":
                    CancelUser();
                    break;
                case "BtnSaveUser":
                    SaveUser();
                    break;
                default:
                    break;
            }
        }
        private void CancelUser()
        {
            Users.Hide();
        }
        private void SaveUser()
        {

        }
    }
}
