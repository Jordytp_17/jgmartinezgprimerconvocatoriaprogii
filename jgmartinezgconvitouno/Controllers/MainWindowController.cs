﻿using jgmartinezgconvitouno.Models;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace jgmartinezgconvitouno.Controllers
{
    class MainWindowController
    {
        private MainWindow Window;
        private SaveFileDialog Savedialog;
        private OpenFileDialog Opendialog;
        private List<string> CurrentItems;

        public MainWindowController(MainWindow window)
        {
            Window = window;
            Savedialog = new SaveFileDialog();
            Opendialog = new OpenFileDialog();
            InitController();
        }

        private void InitController()
        {
            Window.BtnSelect.Click += new RoutedEventHandler(this.MainWindowEventHandler);
            Window.BtnClear.Click += new RoutedEventHandler(this.MainWindowEventHandler);
            Window.BtnSave.Click += new RoutedEventHandler(this.MainWindowEventHandler);
            Window.BtnAddItem.Click += new RoutedEventHandler(this.MainWindowEventHandler);
            CurrentItems = new List<string>();
            Window.ItemList.ItemsSource = CurrentItems;
        }
        private void MainWindowEventHandler(object sender, RoutedEventArgs args)
        {
            Button Btn = (Button)sender;
            switch (Btn.Name)
            {
                case "BtnSelect":
                    Select();
                    break;
                case "BtnSave":
                    save();
                    break;
                case "BtnClear":
                    Clear();
                    break;
                case "BtnAddItem":
                    AddItem();
                    break;
                default:
                    break;
            }
        }
        private void Select()
        {
            Opendialog.Filter = "Xml File (*xml)|*.xml";
            if (Opendialog.ShowDialog() == true)
            {
                ItemGroup items = new ItemGroup().ReadObjectFromXml(Opendialog.FileName);
                CurrentItems.AddRange(items.Items);
                Window.ItemList.Items.Refresh();
                Window.TxtPedido.Text = items.Pedido;
                Window.TxtCliente.Text = items.Cliente;
            }
        }
        private void save()
        {
            string ListName = Window.TxtPedido.Text;
            string ListNameList = Window.TxtCliente.Text;

            if (!string.IsNullOrWhiteSpace(ListName) && !string.IsNullOrWhiteSpace(ListNameList))
            {
                Savedialog.Filter = "Xml File (*.xml)|*.xml";
                if (Savedialog.ShowDialog() == true)
                {
                    ItemGroup items = new ItemGroup();
                    items.Pedido = ListName;
                    items.Cliente = ListNameList;
                    items.Items = CurrentItems;
                    items.WriteObjectFromXml(Savedialog.FileName);
                    Clear();
                }
            }
            else
            {
                MessageBox.Show("Se le ha olvidado llenar algún campo!!!!");
            }
        }
        private void AddItem()
        {
            string NameItem = Window.CmbList.Text;
            if (!string.IsNullOrWhiteSpace(NameItem))
            {
                if (!CurrentItems.Contains(NameItem))
                {
                    CurrentItems.Add(NameItem);
                    Window.ItemList.Items.Refresh();
                    Window.CmbList.Text = "";
                }
                else
                {
                    MessageBox.Show("El item ya existe");
                }
            }
        }
        private void Clear()
        {
            Window.TxtPedido.Text = "";
            Window.TxtCliente.Text = "";
            Window.CmbList.Text = "";
            CurrentItems.Clear();
            Window.ItemList.Items.Refresh();
        }

    }
}
