﻿using jgmartinezgconvitouno.Interfaces;
using jgmartinezgconvitouno.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace jgmartinezgconvitouno.Models
{
    public class ItemGroup : IOFile<ItemGroup>
    {
        public string Pedido { get; set; }
        public string Cliente { get; set; }
        public List<string> Items { get; set; }
        public ItemGroup ReadObjectFromXml(string filePath)
        {
            return XmlSerialization.ReadFromXmlFile<ItemGroup>(filePath);
        }

        public void WriteObjectFromXml(string filePath)
        {
            XmlSerialization.WriteToXmlFile(filePath, this);
        }
    }
}
